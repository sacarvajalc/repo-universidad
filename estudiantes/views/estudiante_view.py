from rest_framework import views
from rest_framework.response import Response
from estudiantes.models.estudiante import Estudiante
from estudiantes.serializers.estudiante_serializer import MostarEstudianteSerializer, CrearEstudianteSerializer


class EstudianteView(views.APIView):
    def get(self, request):
        lista_estudiantes = Estudiante.objects.all()
        serializer = MostarEstudianteSerializer(lista_estudiantes, many=True)
        return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = CrearEstudianteSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "estudiante creado"}, 200)
        else:
            return Response(serializer.errors, 405)

    def delete(self, request, pk):
        try:
            estudiante = Estudiante.objects.get(pk=pk)
            estudiante.delete()
            return Response({"mensaje": "estudiante borrado"}, 200)
        except:
            return Response({"mensaje": "estudiante no existe"}, 400)
