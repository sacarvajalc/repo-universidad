from rest_framework import serializers
from estudiantes.models.estudiante import Estudiante


class CrearEstudianteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estudiante
        fields = ["nombre", "cedula"]


class MostarEstudianteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estudiante
        fields = ["id", "nombre", "cedula"]
