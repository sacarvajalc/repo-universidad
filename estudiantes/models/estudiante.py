from django.db import models

class Estudiante(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60, null=False)
    cedula = models.IntegerField(null=False)